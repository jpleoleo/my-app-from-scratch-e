import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GlobalDataService } from './services/global-data.service';
import { GlobalCommunicateService } from './services/global-communicate.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app-from-scratch-a';

  param = {
    title: 'Initial Title'
  }

  count = 0;

  constructor(private router: Router, private globalDataService: GlobalDataService, private globalCommunicationService:GlobalCommunicateService) {

  }

  ngOnInit() {
    if (this.globalDataService.data.validUser) {
      this.router.navigateByUrl('page-1');
    } else {
      this.router.navigateByUrl('login');
    }
  }

  updateTitleInChild() {
    this.count++;
    this.param.title = 'New Title Updated Count = ' + this.count;
    //this.param = JSON.parse(JSON.stringify(this.param));
  }

  updateTitleInPage1ByGlobalCommunicationService(){
    this.count++;
    this.globalCommunicationService.sendData({from:'app-component', to:'page1', title:'New Title Updated Count = ' + this.count});
  }

  updateTitleInLazyByGlobalCommunicationService(){
    this.count++;
    this.globalCommunicationService.sendData({from:'app-component', to:'lazy', title:'New Title Updated Count = ' + this.count});
  }
}
