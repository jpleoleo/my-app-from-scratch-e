import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyComponent } from './lazy.component';
import { RouterModule, Routes } from '@angular/router';
import { NavigationModule } from '../navigation/navigation.module';
import { LocalDataService } from '../../services/local-data.service';

const routes: Routes = [
  { path: '', redirectTo: 'load-me' },
  { path: 'load-me', component: LazyComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavigationModule
  ],
  declarations: [LazyComponent],
  providers:[LocalDataService]
})
export class LazyModule { }
