import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';
import { HttpClientService } from '../../services/http-client.service';
import { GlobalCommunicateService } from '../../services/global-communicate.service';

@Component({
  selector: 'app-lazy1',
  templateUrl: './lazy1.component.html',
  styleUrls: ['./lazy1.component.css']
})
export class Lazy1Component implements OnInit {

  data:any = [];

  openCloseStatus = 'null';
  allowAnim = false;

  title = 'Lazy-1 Module'

  constructor(private globalDataService:GlobalDataService, private httpClientService:HttpClientService, private globalCommunicationService:GlobalCommunicateService) { }

  ngOnInit() {
    this.globalDataService.data.currentPage = 'lazy-1';
    this.globalDataService.data.pages.lazy1.visited = true;
    this.globalDataService.data.pages.lazy1.visitCount++;

    if(this.globalDataService.data.pages.lazy.data){
      this.data = JSON.parse(JSON.stringify(this.globalDataService.data.pages.lazy.data));
      console.log('data captured from global data');
    }else{
      this.httpClientService.getJSON('assets/data/data.json').subscribe((result)=>{
        this.globalDataService.data.pages.lazy.data = result;
        this.data = JSON.parse(JSON.stringify(this.globalDataService.data.pages.lazy.data));
        console.log('data captured from server');
      });
    }

    this.globalCommunicationService.receiveData.subscribe((result)=>{
      console.log(result);
      this.title = result;
    })
  }

  openClose(event){
    this.allowAnim = true;
    if(this.openCloseStatus==='close' || this.openCloseStatus==='null'){
      this.openCloseStatus='open';
    }else{
      this.openCloseStatus='close';
    }
  }

}
