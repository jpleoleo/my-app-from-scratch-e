import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Lazy1Component } from './lazy1.component';
import { Routes, RouterModule } from '@angular/router';
import { NavigationModule } from '../navigation/navigation.module';
import { LocalDataService } from '../../services/local-data.service';

const routes: Routes = [
  { path: '', redirectTo: 'load-me' },
  { path: 'load-me', component: Lazy1Component }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    NavigationModule
  ],
  declarations: [Lazy1Component],
  providers:[LocalDataService]
})
export class Lazy1Module { }
