import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';
import { GlobalCommunicateService } from '../../services/global-communicate.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class Page1Component implements OnInit {

  @Input('param') private param:any = {
    title:'Initial Title'
  }

  constructor(private globalDataService:GlobalDataService, private globalCommunicationService:GlobalCommunicateService) { }

  ngOnInit() {
    this.globalDataService.data.currentPage = 'page-1';
    this.globalDataService.data.pages.page1.visited = true;
    this.globalDataService.data.pages.page1.visitCount++;

    this.globalCommunicationService.receiveData.subscribe((result)=>{
      console.log(result);
      this.param.title = result;
    })
  }

}
