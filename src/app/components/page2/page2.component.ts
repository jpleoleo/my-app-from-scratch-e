import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {

  constructor(private globalDataService:GlobalDataService) { }

  ngOnInit() {
    this.globalDataService.data.currentPage = 'page-2';
    this.globalDataService.data.pages.page2.visited = true;
    this.globalDataService.data.pages.page2.visitCount++;
  }

}
