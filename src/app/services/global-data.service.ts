/**
 * This service only import in  root module. Should not import in sub or lazy modules
 */
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GlobalDataService {
  data = {
    validUser:false,
    userDetails:{
      name:''
    },
    pages:{
      page1:{
        visited:false,
        visitCount:0,
        data:null
      },
      page2:{
        visited:false,
        visitCount:0,
        data:null
      },
      lazy:{
        visited:false,
        visitCount:0,
        data:null
      },
      lazy1:{
        visited:false,
        visitCount:0,
        data:null
      }
    },
    currentPage:'login'
  }

  initialData = JSON.parse(JSON.stringify(this.data))

  constructor() { }
}
