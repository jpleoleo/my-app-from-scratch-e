/**
 * This service only import in sub and lazy modules. Should not import in root module
 */
import { Injectable } from '@angular/core';

@Injectable()

export class LocalDataService {
  data = {
    subComponent1:{

    },
    subComponent2:{
      
    },
    subComponent3:{
      
    }
  }

  initialData = JSON.parse(JSON.stringify(this.data))

  constructor() { }
}
